<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:layout title="Catálogo de produtos">
	<jsp:body>
		<h1 class="page-header">Catálogo de produtos</h1>
		<table class="table">
			<thead>
				<th>Descrição</th>
				<th>Valor</th>
				<th>Ações</th>
			</thead>
			<tbody>
				<c:forEach var="produto" items="${produtos}">
					<tr>
						<td>${produto.descricaoCurta}</td>
						<td>R$ ${produto.preco}</td>
						<td>
							<a class="btn btn-default" href="${pageContext.request.contextPath}/pedido/adicionar?id=${produto.itemID}">Comprar</a>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</jsp:body>
</t:layout>