<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:layout title="Finalizar compra">
	<jsp:body>
		<h1 class="page-header">Finalizar compra</h1>
		<h2>Lista de produtos deste pedido</h2>
		<table class="table">
			<thead>
				<th>Descrição</th>
				<th>Quantidade</th>
				<th>Valor</th>
			</thead>
			<tbody>
				<c:forEach var="produto" items="${produtos}">
					<tr>
						<td>${produto.descricaoCurta}</td>
						<td>${produto.numItens}</td>
						<td>R$ ${produto.precoTotal}</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<h3>Total R$ ${valorTotal}</h3>
		<a href="#">Link para pagamento</a>
	</jsp:body>
</t:layout>