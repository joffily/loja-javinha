<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<t:layout title="Carrinho">
	<jsp:body>
		<h1 class="page-header">Carrinho de compras</h1>
		<table class="table">
			<thead>
				<th>Descrição</th>
				<th>Quantidade</th>
				<th>Valor</th>
			</thead>
			<tbody>
				<c:forEach var="produto" items="${produtos}">
					<tr>
						<td>${produto.descricaoCurta}</td>
						<td>${produto.numItens}</td>
						<td>R$ ${produto.precoTotal}</td>
					</tr>
				</c:forEach>
				<c:if test="${fn:length(produtos) eq 0}">
					<tr>
						<td>Seu carrinho de compras está vazio</td>
						<td></td>
					</tr>
				</c:if>
			</tbody>
		</table>
		
		<a class="btn btn-success" href="${pageContext.request.contextPath}/catalogo">Continuar comprando</a>
		<c:if test="${fn:length(produtos) gt 0}">
			<a class="btn btn-primary" href="${pageContext.request.contextPath}/pedido/finalizar">Finalizar pedido</a>
		</c:if>
	</jsp:body>
</t:layout>