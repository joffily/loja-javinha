<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:layout>
	<jsp:body>
		<div class="jumbotron">
        	<h1>Loja Javinha</h1>
        	<p class="lead">Implementação de um carrinho de compras utilizando a tecnologia de Servlets.</p>
        	<p><a class="btn btn-lg btn-success" href="https://github.com/joffilyfe/programacao-para-web" role="button">Código fonte</a></p>
      	</div>
	</jsp:body>
</t:layout>