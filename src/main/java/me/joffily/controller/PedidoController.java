package me.joffily.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import br.edu.ifpb.pweb.carrinho.model.Carrinho;
import br.edu.ifpb.pweb.carrinho.model.Catalogo;
import br.edu.ifpb.pweb.carrinho.model.Item;
import br.edu.ifpb.pweb.carrinho.model.ItemCarrinho;

public class PedidoController {

	public RequestDispatcher index(HttpServletRequest request, HttpServletResponse response) {
		Carrinho carrinho = obterCarrinho(request.getSession(), request);
		request.setAttribute("produtos", carrinho.getItemsCarrinho());

		RequestDispatcher dispatcher = request.getRequestDispatcher("/view/pedido/index.jsp");
		return dispatcher;
	}
	
	/*
	 * Adiciona um item ao carrinho de compras
	 * Redireciona para a Index do carrinho
	 */
	public RequestDispatcher adicionar(HttpServletRequest request, HttpServletResponse response) throws IOException {

		HttpSession session = request.getSession();
		Carrinho carrinho = obterCarrinho(session, request);

		Catalogo catalogo = new Catalogo();
		Item item = catalogo.getItem(request.getParameter("id"));
		
		if (item == null) {
			response.sendRedirect(request.getServletContext().getContextPath() + "/catalogo");
		} else {
			carrinho.adicioneItem(item.getItemID());
		}

		response.sendRedirect(request.getServletContext().getContextPath() + "/pedido");
		return null;
	}

	/*
	 * Finaliza o pedido do cliente
	 */
	public RequestDispatcher finalizar(HttpServletRequest request, HttpServletResponse response) throws IOException {
		HttpSession session = request.getSession();
		Carrinho carrinho = obterCarrinho(session, request);
		Double total = 0.0;
		
		// Se o carrinho estiver vazio, envie para o catálogo
		if (carrinho.getItemsCarrinho().size() == 0) {
			response.sendRedirect(request.getServletContext().getContextPath() + "/catalogo");
		}

		for (ItemCarrinho item : carrinho.getItemsCarrinho()) {
			total += item.getPrecoTotal();
		}
		
		request.setAttribute("valorTotal", total);
		request.setAttribute("produtos", carrinho.getItemsCarrinho());
		
		// Limpa a session
		if (!session.isNew()) {
			session.invalidate();
		}
		
		/*
		 * Podemos adicionar mais lógica do negócio aqui ou usando o padrão facade para
		 * gerar o binário, salvar, enviar por e-mail, etc..
		 */

		RequestDispatcher dispatcher = request.getRequestDispatcher("/view/pedido/finalizar.jsp");

		return dispatcher;
	}

	/*
	 * Obtem um carrinho de compras (cria ou recupera)
	 */
	private Carrinho obterCarrinho(HttpSession session, HttpServletRequest request) {
		session = request.getSession();
		Carrinho carrinho;

		// Se o cliente já possuir um carrinho, atribua ele a variável
		if (!session.isNew() && session.getAttribute("carrinho") != null) {
			carrinho = (Carrinho) session.getAttribute("carrinho");
		} else {
			carrinho = new Carrinho();
			session.setAttribute("carrinho", carrinho);
		}

		return carrinho;
	}
}
