package me.joffily.controller;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.edu.ifpb.pweb.carrinho.model.Catalogo;

public class CatalogoController {

	public RequestDispatcher index(HttpServletRequest request, HttpServletResponse response) {
		request.setAttribute("produtos", Catalogo.getItems());
		RequestDispatcher dispatcher = request.getRequestDispatcher("/view/catalogo/index.jsp");
		return dispatcher;
	}	
}
